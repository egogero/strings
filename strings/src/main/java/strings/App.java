package strings;

import java.util.regex.Pattern;

/**
 * Hello world!
 */
public final class App {

    /**
     * Write a program (method) which will receive two strings and do the following:
     *
     * 1.) print if one string is part of another:
     *
     * for example:
     *
     * String a = Category
     *
     * String b = Cat
     *
     * compareStrings(a, b)
     *
     * output
     *
     * "Cat is the part of Category"
     *
     *
     *
     * 2.) If Strings are equal, the expected print is:
     *
     * "The Strings are the same"
     *
     *
     *
     * 3.) If one of the strings is a null - print:
     *
     * "Cannot compare Strings"
     *
     *
     */
    public static void main(String[] args) {

        // compareStrings("Java","Java collection");

        System.out.println("PalindROme : " + palidromCheck("racecar"));

        // System.out.println( palidromCheck("arosaupalanalapuasora"));

        // System.out.println( addChars("dog"));

        // System.out.println( maxStart("paz snacks") );

        // System.out.println(fizzString("noChange"));

        // System.out.println(bigThere("dfbdfggg"));

        // System.out.println(stringTimes("Hi",1));

        System.out.println(isPlural("dudesfs"));

    }

    static void compareStrings(String str1, String str2) {

        if (str1.isEmpty() || str2.isEmpty()) {
            System.out.printf("Cannot compare Strings\n");
        } else if (str1.toLowerCase().contains(str2.toLowerCase()) || str2.toLowerCase().contains(str1.toLowerCase())) {

            System.out.printf("[ %s ] is the part of [ %s ] \n", str1, str2);
        } else if (str1.equals(str2)) {
            System.out.printf("The Strings [ %s ], [ %s ] are the same\n", str1, str2);
        } else {
            System.out.printf("The Strings [ %s ], [ %s ] are NOT the same\n", str1, str2);
        }
    }

    static boolean palidromCheck(String str) {

        String reversedString = "";

        for (int i = str.length() - 1; i >= 0; i--) {
            reversedString += str.charAt(i);
        }
        // System.out.println("original >> "+ str+"\nreversedString >>
        // "+reversedString);
        return (str.equals(reversedString));
    }

    // Need more exercise?

    // Given a string, take the last char and
    // return a new string with the last char added at the beginning and the end,
    // so "dog" will become "gdogg". The original string should be length 1 or more.

    public static String addChars(String str) {

        return str = str.charAt(str.length() - 1) + str + str.charAt(str.length() - 1);
    }

    // Return true if the given string begins with "max",
    // except the 'm' can be anything, so "pax", "9ax" .. all count.

    public static boolean maxStart(String str) {

        if ((str.toLowerCase().contains("max")) || (str.toLowerCase().contains("ax")))
            return true;
        else
            return false;

    }

    // Given string str, if the string starts with "f"
    // return "Fizz". If the string ends with "b" return "Buzz".
    // If both the "f" and "b" conditions are true, return "FizzBuzz".
    // In all other cases, return the string unchanged

    public static String fizzString(String str) {

        if ((str.charAt(0) == 'f') && (str.charAt(str.length() - 1) == 'b')) {
            return "FizzBuzz";
        }

        if (str.charAt(0) == 'f') {
            return "Fizz";
        }

        if (str.charAt(str.length() - 1) == 'b') {
            return "Buzz";
        }

        return str;

    }

    // Return true if the given string contains a "big" string,
    // but where the middle 'i' char can be any char.

    public static boolean bigThere(String str) {

        // http://tutorials.jenkov.com/java-regex/index.html

        final String regex = ".*b.g.*";
        return Pattern.matches(regex, str);
    }

    // Given a string and a non-negative int n,
    // return a larger string that is n copies of the original string.

    public static String stringTimes(String str, int times) {

        for (int i = 1; i < times; i++) {
            str += str;
        }
        return str;
    }

    // Create a function that takes in a word and
    // determines whether or not it is plural.
    // A plural word is one that ends in "s".

    public static boolean isPlural(String str) {
        // alternative solutions

        // return Pattern.matches(".*s", str);

        return str.endsWith("s");
    }

}
